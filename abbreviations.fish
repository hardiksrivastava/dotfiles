#!/usr/bin/fish
# run this fish script to add all abbreviations
abbr -a -U l ls
abbr -a -U update sudo pacman -Syu
abbr -a -U pinstall sudo pacman -S
abbr -a -U yinstall yay -S 
abbr -a -U premove sudo pacman -Rns 
abbr -a -U yremove yay -Rns  
abbr -a -U gaddall git add --all
abbr -a -U gadd git add 
abbr -a -U gcommit git commit -m 
